import Home from "./pages/Home.vue";
import Remote1App from "./pages/Remote1App.vue";
import Remote2App from "./pages/Remote2App.vue";


const routes = [
  { path: "/",
    component: Home
  },
  { path: "/app_1",
    component: Remote1App
  },
  { path: "/app_2",
    component: Remote2App
  }
];


export function setup_router(VueRouter, prefix) {
  return new VueRouter({
    mode: "history",
    base: prefix,
    routes: routes
  });
}
