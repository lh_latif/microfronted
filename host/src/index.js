import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import { setup_router } from "./router.js";

Vue.use(VueRouter)

const app = new Vue({
  render: r => r(App),
  router: setup_router(VueRouter, "/")
}).$mount("#app");
