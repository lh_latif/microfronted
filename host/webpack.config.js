const { ModuleFederationPlugin } = require("webpack").container;
const {VueLoaderPlugin} = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

const hostContainer = new ModuleFederationPlugin({
  name: "main",
  remotes: {
    "remote1": "remote1@http://localhost:3345/app1.js",
    "remote2": "remote2@http://localhost:3346/app2.js"
  }
});

module.exports = {
  entry: path.resolve(__dirname, "./src/index.js"),
  plugins: [
    hostContainer,
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "index.html")
    })
  ],
  devServer: {
    contentBase: path.resolve(__dirname, "dist"),
    historyApiFallback: true,
    publicPath: "/",
    hot: true,
    port: 3344
  },
  module: {
    rules: [
      { test: /\.js$/,
        exclude: [/node_modules/],
        loader: "babel-loader"
      },
      { test: /\.vue$/,
        loader: "vue-loader"
      }
    ]
  }
};
