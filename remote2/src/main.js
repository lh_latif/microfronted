import Vue from "vue";
import VueRouter from "vue-router";
import { setup_router } from "./router.js";
import App from "./App.vue";

Vue.use(VueRouter);

export function start_app(element, prefix) {
  return new Vue({
    render: v => v(App),
    router: setup_router(VueRouter, prefix)
  }).$mount(element);
}


export function stop_app(instance) {
  console.log(instance);
  instance.$destroy();
  instance.$forceUpdate();
}
