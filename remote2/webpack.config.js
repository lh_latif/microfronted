const { ModuleFederationPlugin } = require("webpack").container;
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

const appContainer = new ModuleFederationPlugin({
  name: "remote2",
  filename: "app2.js",
  exposes: {
    "./main.js": "./src/main.js"
  }
});

module.exports = {
  entry: path.resolve(__dirname, "./src/index.js"),
  plugins: [
    appContainer,
    new VueLoaderPlugin(),
    // new HtmlWebpackPlugin(),
  ],
  module: {
    rules: [
      { test: /\.js$/,
        exclude: [/node_modules/],
        loader: "babel-loader"
      },
      { test: /\.vue$/,
        loader: "vue-loader"
      }
    ]
  },
  devServer: {
    port: 3346
  }
};
