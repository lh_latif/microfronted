const path = require("path");
const { ModuleFederationPlugin } = require("webpack").container;
const HtmlWebpackPlugin = require("html-webpack-plugin");
const deps = require("./package.json").dependencies;

const moduleFederationPlugin = new ModuleFederationPlugin({
  name: "remote1",
  filename: "app1.js",
  exposes: {
    "./main.js": "./src/main.js"
  },
  shared: ["react-dom", "react"]
});

module.exports = {
  entry: path.resolve(__dirname, "./src/index.js"),
  module: {
    rules: [
      { test: /\.js$/,
        exclude: [/node_modules/],
        use: "babel-loader"
      },
    ]
  },
  plugins: [
    moduleFederationPlugin,
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./index.html")
    })
  ],
  devServer: {
    contentBase: path.resolve(__dirname, "./dist"),
    historyApiFallback: true,
    port: 3345
  }
}
