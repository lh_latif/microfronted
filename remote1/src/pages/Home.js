import React from "react";

class Home extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <h1>Ini Home</h1>
        <button onClick={() => this.props.history.push("/app")}>
          go to app
        </button>
      </div>
    );
  }
}


export default Home;
