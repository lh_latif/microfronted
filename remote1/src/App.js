import React from "react";
import {getAppPrefix} from "./var.js";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Home from "./pages/Home.js";


class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <div>
          <h1>React App</h1>
        </div>
        <Router basename={getAppPrefix()}>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/app">
              <h2>Ini App</h2>
            </Route>
          </Switch>
        </Router>
      </>
    );
  }
}


export default App;
