import ReactDOM from "react-dom";
import React from "react";
import App from "./App.js";
import { setAppPrefix } from "./var.js";

export function start_app(element, prefix) {
  setAppPrefix(prefix);
  ReactDOM.render(
    <div>
      <App/>
    </div>,
    element
  );
}


export function stop_app(element) {
  ReactDOM.unmountComponentAtNode(element);
}
