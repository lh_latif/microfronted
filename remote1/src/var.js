var $prefix = "/";

export function setAppPrefix(prefix) {
  $prefix = prefix;
}

export function getAppPrefix() {
  return $prefix;
}
